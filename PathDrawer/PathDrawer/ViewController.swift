//
//  ViewController.swift
//  PathDrawer
//
//  Created by Kathryn Lovett on 8/17/16.
//  Copyright © 2016 Kathryn Lovett. All rights reserved.
//


import UIKit


class ViewController: UIViewController {
    private var allPoints: Array<Array<Dictionary<String, CGFloat>>> = []
    
    @IBAction private func test(sender: UIBarButtonItem) {
        if pathTouchView.hidden {
            pathView.hidden = true
            pathTouchView.hidden = false
            
            sender.title = "Draw"
        }
        else {
            pathView.hidden = false
            pathTouchView.hidden = true
            
            sender.title = "Test"
        }
    }
    
    @IBAction private func save(sender: AnyObject) {
        let points = pathView.points
        
//        allPoints.append(points.map({ ["x" : $0.x, "y" : $0.y] }))
        
        var plist: Array<Dictionary<String, CGFloat>> = []
        for point in points {
            let pointValues = ["x" : point.x, "y" : point.y]
            plist.append(pointValues)
        }
        
        allPoints.append(plist)
        
        let path = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true).first!
        let plistPath = (path as NSString).stringByAppendingPathComponent("paths.plist")
        (allPoints as NSArray).writeToFile(plistPath, atomically: true)
        print("Writing file to \(plistPath)")
        
        pathView.reset()

        updatePathTouchView()
    }
    
    private func updatePathTouchView() {
        let path = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true).first!
        let plistPath = (path as NSString).stringByAppendingPathComponent("paths.plist")
        let allPointValues = NSArray(contentsOfFile: plistPath) as! Array<Array<Dictionary<String, CGFloat>>>
        
//        let allPathValues = allPointValues.map({ $0.map({ CGPoint(x: $0["x"]!, y: $0["y"]!) }) })
        
        var allPathValues: Array<Array<CGPoint>> = []
        for pointValues in allPointValues {
            var pathValues: Array<CGPoint> = []
            for singlePointValues in pointValues {
                let point = CGPoint(x: singlePointValues["x"]!, y: singlePointValues["y"]!)
                pathValues.append(point)
            }
            
            allPathValues.append(pathValues)
        }
        
        pathTouchView.allPathValues = allPathValues
    }
    
    @IBOutlet private weak var pathView: PathView!
    @IBOutlet private weak var pathTouchView: PathTouchView!
}

