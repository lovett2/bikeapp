//
//  PathView.swift
//  PathDrawer
//
//  Created by Kathryn Lovett on 8/17/16.
//  Copyright © 2016 Kathryn Lovett. All rights reserved.
//

import UIKit

class PathView: UIView {
    func reset() {
        points = []
    }
    
    var points: Array<CGPoint> = [] {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBAction func tap(sender: UITapGestureRecognizer) {
        points.append(sender.locationInView(self))
    }
    
    override func drawRect(rect: CGRect) {
        guard points.count > 1 else {
            return
        }
        
        let path = UIBezierPath()
        
        path.moveToPoint(points[0])
        
        for point in points[1..<points.count] {
            path.addLineToPoint(point)
        }
        
        path.closePath()
        
        UIColor.clearColor().setStroke()
        UIColor(red: 0.0, green: 0.8, blue: 0.0, alpha: 0.5).setFill()

        path.fill()
    }
}
