//
//  Bike.swift
//  FinalProject
//
//  Created by Kathryn Lovett on 8/20/16.
//
//

import Foundation
import CoreData
import CoreDataService


class Bike: NSManagedObject, NamedEntity {

    static var entityName: String {
        return "Bike"
    }


}
