//
//  Bike+CoreDataProperties.swift
//  FinalProject
//
//  Created by Kathryn Lovett on 8/20/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Bike {

    @NSManaged var year: NSNumber?
    @NSManaged var model: String?
    @NSManaged var type: String?
    @NSManaged var wheel: String?
    @NSManaged var shifter: String?
    @NSManaged var gears: String?
    @NSManaged var photo: Photo?
    @NSManaged var repair: NSSet?

}
