//
//  InstructionViewController.swift
//  FinalProject
//
//  Created by Kathryn Lovett on 8/21/16.
//
//

import UIKit

class InstructionViewController: UIViewController {
    var selectedRepair: Repair!
    @IBOutlet var textView: UITextView!
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationItem.title = selectedRepair.name
        
        textView.text = selectedRepair.instructions
    }
}
