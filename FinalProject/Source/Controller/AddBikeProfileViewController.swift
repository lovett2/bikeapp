//
//  AddBikeProfileViewController.swift
//  FinalProject
//
//  Created by Kathryn Lovett on 8/4/16.
//
//

import UIKit
import MobileCoreServices

class AddBikeProfileViewController: UITableViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    weak var delegate: AddBikeProfileViewControllerDelegate!
    
    @IBAction dynamic func cancel(sender: AnyObject) {
        delegate.addBikeProfileViewControllerDidFinish()
    }

    @IBAction func saveButtonPushed(sender: AnyObject) {
        do {
            try BikeService.sharedBikeService.addBikeProfile(model, type: type, year: year, andPhoto: photo)
            
            delegate.addBikeProfileViewControllerDidFinish()
        }
        catch _ {
            let alertController = UIAlertController(title: "Save failed", message: "Failed to save the new bike profile", preferredStyle: .Alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
            presentViewController(alertController, animated: true, completion: nil)

        }
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch BikeItemRow.Items[indexPath.row] {
        case .Model:
            modelTextField.becomeFirstResponder()
        case .Type:
            typeTextField.becomeFirstResponder()
        case .Year:
            yearTextField.becomeFirstResponder()
        case .Photo:
            if photo == nil {
                let alertController = UIAlertController(title: nil, message: "Pick Image Source", preferredStyle: .ActionSheet)
                
                let checkSourceType = { (sourceType: UIImagePickerControllerSourceType, buttonText: String) -> Void in
                    if UIImagePickerController.isSourceTypeAvailable(sourceType) {
                        alertController.addAction(UIAlertAction(title: buttonText, style: .Default, handler: self.imagePickerControllerSourceTypeActionHandlerForSourceType(sourceType)))
                    }
                }
                checkSourceType(.Camera, "Camera")
                checkSourceType(.PhotoLibrary, "Photo Library")
                checkSourceType(.SavedPhotosAlbum, "Saved Photos Album")
                
                if !alertController.actions.isEmpty {
                    alertController.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
                    
                    presentViewController(alertController, animated: true, completion: nil)
                }
            }
        }
        
    }
    
    // MARK: UIImagePickerControllerDelegate
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        // First check for an edited image, then the original image
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            photo = image
        }
        else if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            photo = image
        }
        
        updateUIForPicture()
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    private func updateUIForPicture(animated animated: Bool = true) {
        if animated {
            if let somePhoto = photo where pictureImageView.hidden {
                pictureImageView.image = somePhoto
                self.pictureImageView.transform = CGAffineTransformRotate(CGAffineTransformMakeScale(0.01, 0.01), CGFloat(-M_PI))
                pictureImageView.hidden = false
                
                UIView.animateWithDuration(0.2, animations: { () -> Void in
                    self.pictureImageView.transform = CGAffineTransformIdentity
                    self.addPictureLabel.alpha = 0.0
                    }, completion: { (complete) -> Void in
                        self.addPictureLabel.alpha = 1.0
                        self.addPictureLabel.hidden = true
                })
            }
            else {
                addPictureLabel.alpha = 0.0
                addPictureLabel.hidden = false
                
                UIView.animateWithDuration(0.2, animations: { () -> Void in
                    self.pictureImageView.transform = CGAffineTransformRotate(CGAffineTransformMakeScale(0.01, 0.01), CGFloat(M_PI))
                    self.addPictureLabel.alpha = 1.0
                    }, completion: { (complete) -> Void in
                        self.pictureImageView.image = nil
                        self.pictureImageView.transform = CGAffineTransformIdentity
                        self.pictureImageView.hidden = true
                })
            }
        }
        else {
            if let somePicture = photo {
                pictureImageView.hidden = false
                pictureImageView.image = somePicture
                addPictureLabel.hidden = true
            }
            else {
                pictureImageView.hidden = true
                pictureImageView.image = nil
                addPictureLabel.hidden = false
            }
        }
    }
    
    private func imagePickerControllerSourceTypeActionHandlerForSourceType(sourceType: UIImagePickerControllerSourceType) -> (action: UIAlertAction) -> Void {
        return { (action) in
            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.sourceType = sourceType
            imagePickerController.mediaTypes = [kUTTypeImage as String]
            imagePickerController.allowsEditing = true
            
            self.presentViewController(imagePickerController, animated: true, completion: nil)
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        modelTextField.text = model
        typeTextField.text = type
        yearTextField.text = String(year)
        
        updateUIForPicture(animated: false)
    }
    
    // MARK: UITextFieldDelegate
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        switch textField {
        case modelTextField:
            model = (textField.text ?? "" as NSString).stringByReplacingCharactersInRange(range, withString: string)
        case typeTextField:
            type = (textField.text ?? "" as NSString).stringByReplacingCharactersInRange(range, withString: string)
        case yearTextField:
            year = (textField.text ?? "" as NSString).stringByReplacingCharactersInRange(range, withString: string)
        default:
            return false
        }
        
        return true
    }
    
    func textFieldShouldClear(textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        switch textField {
        case modelTextField:
            modelTextField.resignFirstResponder()
        case typeTextField:
            typeTextField.resignFirstResponder()
        case yearTextField:
            yearTextField.resignFirstResponder()
        default:
            return false
        }
        
        return false
    }
    

    
    // MARK: Properties (Private)
    private var model = AddBikeProfileViewController.DefaultModel
    private var type = AddBikeProfileViewController.DefaultType
    private var year = AddBikeProfileViewController.DefaultYear
    private var photo: UIImage?

    // MARK: Properties (IBOutlet)
    @IBOutlet private weak var modelTextField: UITextField!
    @IBOutlet private weak var typeTextField: UITextField!
    @IBOutlet private weak var yearTextField: UITextField!
    @IBOutlet private weak var borrowerLabel: UILabel!
    @IBOutlet private weak var pictureImageView: UIImageView!
    @IBOutlet private weak var addPictureLabel: UILabel!
    
    // MARK: Properties (Private Static Constant)
    private static let DefaultModel = "New Bike"
    private static let DefaultType = "Road Bike"
    private static let DefaultYear = "0"
    
    // LentItemRow
    private enum BikeItemRow {
        case Model
        case Type
        case Year
        case Photo
        
        static let Items: Array<BikeItemRow> = [.Model, .Type, .Year, .Photo]
    }
}
