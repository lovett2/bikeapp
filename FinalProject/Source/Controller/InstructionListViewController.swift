//
//  SeatViewController.swift
//  FinalProject
//
//  Created by Kathryn Lovett on 8/18/16.
//
//

import UIKit
import CoreData

class InstructionListViewController: UIViewController, NSFetchedResultsControllerDelegate, UITableViewDelegate, UITableViewDataSource {
    private var fetchedResultsController: NSFetchedResultsController?
    var part: Int?
    var type: String?
    
    @IBOutlet var tableView: UITableView!
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if let bikePart = part {
            switch bikePart {
            case 0:
                type = "seat"
            case 1:
                type = "wheel"
            case 2:
                type = "gears"
            case 3:
                type = "handlebar"
            case 4:
                type = "brake"
            case 5:
                type = "shifter"
            case 6:
                type = "spoke"
            default: break
            }
            
            navigationItem.title = type! + " maintenance"
        }
        
        setupResultsController()
    }
    func setupResultsController() {
        if let partType = type {
            print("Part type is " + partType)
            if let resultsController = try? BikeService.sharedBikeService.fetchedResultsControllerForRepairType(partType) {
                resultsController.delegate = self
                fetchedResultsController = resultsController
            }
            else {
                fetchedResultsController = nil
            }
        }
        
        tableView.reloadData()
    }
    
    func updateCell(cell: UITableViewCell, forRepair repair: Repair) {
        cell.textLabel!.text = repair.name
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let result: Int
        
        if let someSectionInfo = fetchedResultsController?.sections?[section] {
            result = someSectionInfo.numberOfObjects
        }
        else {
            result = 0
        }
        print("Sections = " + String(result))
        return result
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("InstructionCell", forIndexPath: indexPath)
        
        if let someRepair = fetchedResultsController?.objectAtIndexPath(indexPath) as? Repair {
            updateCell(cell, forRepair: someRepair)
        }
        
        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "FullInstructionSegue" {
            if let indexPath = tableView.indexPathForSelectedRow, let selectedRepair = fetchedResultsController?.objectAtIndexPath(indexPath) as? Repair {
                let instructionViewController = segue.destinationViewController as! InstructionViewController
                instructionViewController.selectedRepair = selectedRepair
            }
        }
        else {
            super.prepareForSegue(segue, sender: sender)
        }
    }
}
