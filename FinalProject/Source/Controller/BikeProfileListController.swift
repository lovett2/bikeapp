//
//  BikeProfileListController.swift
//  FinalProject
//
//  Created by Kathryn Lovett on 8/2/16.
//
//

import UIKit
import CoreDataService
import CoreData

class BikeProfileListController: UIViewController, AddBikeProfileViewControllerDelegate, UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate {
    @IBOutlet private var tableView: UITableView!
    
    @IBAction private func unwindFromBikeProfileViewController(sender: UIStoryboardSegue) {
        let bikeProfileViewController = sender.sourceViewController as! BikeProfileViewController
        bikeProfileViewController.save()
    }
    
    private var fetchedResultsController: NSFetchedResultsController?
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        setupResultsController()
    }
    
    func setupResultsController() {
        if let resultsController = try? BikeService.sharedBikeService.fetchedResultsControllerForBikeProfileList() {
            resultsController.delegate = self
            fetchedResultsController = resultsController
        }
        else {
            fetchedResultsController = nil
        }
        
        tableView.reloadData()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let result: Int
        
        if let someSectionInfo = fetchedResultsController?.sections?[section] {
            result = someSectionInfo.numberOfObjects
        }
        else {
            result = 0
        }
        
        return result
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("BikeProfileCell", forIndexPath: indexPath)
        
        if let someBike = fetchedResultsController?.objectAtIndexPath(indexPath) as? Bike {
            updateCell(cell, forBike: someBike)
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        performSegueWithIdentifier("BikeProfileSegue", sender: self)
    }
    
    func updateCell(cell: UITableViewCell, forBike bike: Bike) {
        cell.textLabel!.text = bike.model
        cell.detailTextLabel!.text = bike.type
    }
    
    func addBikeProfileViewControllerDidFinish() {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: NSFetchedResultsControllerDelegate
    func controllerWillChangeContent(controller: NSFetchedResultsController) {
        tableView.beginUpdates()
    }
    
    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
        switch type {
        case .Delete:
            tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Left)
        case .Insert:
            tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Left)
        case .Move:
            if let cell = tableView.cellForRowAtIndexPath(indexPath!), let bike = anObject as? Bike {
                updateCell(cell, forBike: bike)
            }
            tableView.moveRowAtIndexPath(indexPath!, toIndexPath: newIndexPath!)
        case .Update:
            if let cell = tableView.cellForRowAtIndexPath(indexPath!), let bike = anObject as? Bike {
                updateCell(cell, forBike: bike)
            }
        }
    }
    
    func controller(controller: NSFetchedResultsController, didChangeSection sectionInfo: NSFetchedResultsSectionInfo, atIndex sectionIndex: Int, forChangeType type: NSFetchedResultsChangeType) {
        switch type {
        case .Delete:
            tableView.deleteSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Left)
        case .Insert:
            tableView.insertSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Left)
        default:
            print("Unexpected change type in controller:didChangeSection:atIndex:forChangeType:")
        }
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        tableView.endUpdates()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "AddBikeSegue" {
            let navigationController = segue.destinationViewController as! UINavigationController
            let addBikeProfileViewController = navigationController.topViewController as! AddBikeProfileViewController
            addBikeProfileViewController.delegate = self
        }
        else if segue.identifier == "BikeProfileSegue" {
            if let indexPath = tableView.indexPathForSelectedRow, let selectedBike = fetchedResultsController?.objectAtIndexPath(indexPath) as? Bike {
                let bikeProfileViewController = segue.destinationViewController as! BikeProfileViewController
                bikeProfileViewController.selectedBike = selectedBike
            }
        }
        else {
            super.prepareForSegue(segue, sender: sender)
        }
    }
    
    
}
