//
//  BikeProfileViewController.swift
//  FinalProject
//
//  Created by Kathryn Lovett on 8/1/16.
//
//

import UIKit
import CoreData
import CoreDataService
import MobileCoreServices

class BikeProfileViewController: UITableViewController, NSFetchedResultsControllerDelegate, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @IBOutlet var modelLabel: UILabel!
    @IBOutlet var typeLabel: UILabel!
    @IBOutlet var yearLabel: UILabel!
    @IBOutlet var wheelLabel: UILabel!
    @IBOutlet var shifterLabel: UILabel!
    @IBOutlet var gearsLabel: UILabel!
    @IBOutlet var addPhotoLabel: UILabel!
    
    @IBOutlet var modelTextField: UITextField!
    @IBOutlet var typeTextField: UITextField!
    @IBOutlet var yearTextField: UITextField!
    @IBOutlet var wheelTextField: UITextField!
    @IBOutlet var shifterTextField: UITextField!
    @IBOutlet var gearsTextField: UITextField!
    
    @IBOutlet var profilePhoto: UIImageView!
    
    var selectedBike: Bike!
    
    private var fetchedResultsController: NSFetchedResultsController?
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        modelLabel.text = selectedBike.model
        model = selectedBike.model
        modelTextField.text = model
        
        typeLabel.text = selectedBike.type
        type = selectedBike.type
        typeTextField.text = type
        
        if let someYear = selectedBike.year {
            yearLabel.text = String(someYear)
            year = String(someYear)
            yearTextField.text = String(year)
        }
        else {
            yearLabel.text = ""
            yearTextField.text = ""
        }
        
        if let someWheel = selectedBike.wheel {
            wheelLabel.text = String(someWheel)
            wheel = someWheel
            wheelTextField.text = wheel
        }
        else {
            wheelLabel.text = ""
            wheelTextField.text = ""
        }
        
        if let someShifter = selectedBike.shifter {
            shifterLabel.text = someShifter
            shifter = someShifter
            shifterTextField.text = shifter
        }
        else {
            shifterLabel.text = ""
            shifterTextField.text = ""
        }
        
        if let someGears = selectedBike.gears {
            gearsLabel.text = someGears
            gears = someGears
            gearsTextField.text = gears
        }
        else {
            gearsLabel.text = ""
            gearsTextField.text = ""
        }
        
        if let somePhoto = selectedBike.photo {
            photo = UIImage(data: somePhoto.data!)
        }
        
        updateUIForPhoto(animated: true)
        setupResultsController()
    }

    func save() {
        do {
            try BikeService.sharedBikeService.updateBikeProfile(selectedBike, model: model, type: type, year: year, wheel: wheel, shifter: shifter, gears: gears, andPhoto: photo)
        }
        catch _ {
            let alertController = UIAlertController(title: "Save failed", message: "Failed to save the updated bike profile", preferredStyle: .Alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
            presentViewController(alertController, animated: true, completion: nil)
            
        }
    }
    
    func setupResultsController() {
        if let resultsController = try? BikeService.sharedBikeService.fetchedResultsControllerForBikeProfileList() {
            resultsController.delegate = self
            fetchedResultsController = resultsController
        }
        else {
            fetchedResultsController = nil
        }
        
        tableView.reloadData()
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        switch BikeRow.Items[indexPath.row] {
        case .Model:
            if modelTextField.hidden == true {
                modelTextField.text = model
                modelTextField.hidden = false
                modelLabel.hidden = true
            }
            else {
                modelLabel.text = model
                modelTextField.hidden = true
                modelLabel.hidden = false
            }
        case .Type:
            if typeTextField.hidden == true {
                typeTextField.text = type
                typeTextField.hidden = false
                typeLabel.hidden = true
            }
            else {
                typeLabel.text = type
                typeTextField.hidden = true
                typeLabel.hidden = false
            }
        case .Year:
            if yearTextField.hidden == true {
                yearTextField.text = year
                yearTextField.hidden = false
                yearLabel.hidden = true
            }
            else {
                yearLabel.text = year
                yearTextField.hidden = true
                yearLabel.hidden = false
            }
        case .Wheel:
            if wheelTextField.hidden == true {
                wheelTextField.text = wheel
                wheelTextField.hidden = false
                wheelLabel.hidden = true
            }
            else {
                wheelLabel.text = wheel
                wheelTextField.hidden = true
                wheelLabel.hidden = false
            }
        case .Shifter:
            if shifterTextField.hidden == true {
                shifterTextField.text = shifter
                shifterTextField.hidden = false
                shifterLabel.hidden = true
            }
            else {
                shifterLabel.text = shifter
                shifterTextField.hidden = true
                shifterLabel.hidden = false
            }
        case .Gears:
            if gearsTextField.hidden == true {
                gearsTextField.text = gears
                gearsTextField.hidden = false
                gearsLabel.hidden = true
            }
            else {
                gearsLabel.text = gears
                gearsTextField.hidden = true
                gearsLabel.hidden = false
            }
        case .Photo:
            if photo == nil {
                let alertController = UIAlertController(title: nil, message: "Pick Image Source", preferredStyle: .ActionSheet)
                
                let checkSourceType = { (sourceType: UIImagePickerControllerSourceType, buttonText: String) -> Void in
                    if UIImagePickerController.isSourceTypeAvailable(sourceType) {
                        alertController.addAction(UIAlertAction(title: buttonText, style: .Default, handler: self.imagePickerControllerSourceTypeActionHandlerForSourceType(sourceType)))
                    }
                }
                checkSourceType(.Camera, "Camera")
                checkSourceType(.PhotoLibrary, "Photo Library")
                checkSourceType(.SavedPhotosAlbum, "Saved Photos Album")
                
                if !alertController.actions.isEmpty {
                    alertController.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
                    
                    presentViewController(alertController, animated: true, completion: nil)
                }
            }
        }
    
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
    }
    
    // MARK: UITextFieldDelegate
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        switch textField {
        case modelTextField:
            model = (textField.text ?? "" as NSString).stringByReplacingCharactersInRange(range, withString: string)
        case typeTextField:
            type = (textField.text ?? "" as NSString).stringByReplacingCharactersInRange(range, withString: string)
        case yearTextField:
            year = (textField.text ?? "" as NSString).stringByReplacingCharactersInRange(range, withString: string)
        case wheelTextField:
            wheel = (textField.text ?? "" as NSString).stringByReplacingCharactersInRange(range, withString: string)
        case shifterTextField:
            shifter = (textField.text ?? "" as NSString).stringByReplacingCharactersInRange(range, withString: string)
        case gearsTextField:
            gears = (textField.text ?? "" as NSString).stringByReplacingCharactersInRange(range, withString: string)
        default:
            return false
        }
        
        return true
    }
    
    func textFieldShouldClear(textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        switch textField {
        case modelTextField:
            modelTextField.resignFirstResponder()
        case typeTextField:
            typeTextField.resignFirstResponder()
        case yearTextField:
            yearTextField.resignFirstResponder()
        case wheelTextField:
            wheelTextField.resignFirstResponder()
        case shifterTextField:
            shifterTextField.resignFirstResponder()
        case gearsTextField:
            gearsTextField.resignFirstResponder()
        default:
            return false
        }
        
        return false
    }
    
    // MARK: UIImagePickerControllerDelegate
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        // First check for an edited image, then the original image
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            photo = image
        }
        else if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            photo = image
        }
        
        updateUIForPhoto(animated: true)
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    private func updateUIForPhoto(animated animated: Bool = true) {
        if animated {
            if let somePhoto = photo {
                profilePhoto.image = somePhoto
                self.profilePhoto.transform = CGAffineTransformRotate(CGAffineTransformMakeScale(0.01, 0.01), CGFloat(-M_PI))
                profilePhoto.hidden = false
                
                UIView.animateWithDuration(0.2, animations: { () -> Void in
                    self.profilePhoto.transform = CGAffineTransformIdentity
                    self.addPhotoLabel.alpha = 0.0
                    }, completion: { (complete) -> Void in
                        self.addPhotoLabel.alpha = 1.0
                        self.addPhotoLabel.hidden = true
                })

            }
            else {
                addPhotoLabel.alpha = 0.0
                addPhotoLabel.hidden = false
                
                UIView.animateWithDuration(0.2, animations: { () -> Void in
                    self.profilePhoto.transform = CGAffineTransformRotate(CGAffineTransformMakeScale(0.01, 0.01), CGFloat(M_PI))
                    self.addPhotoLabel.alpha = 1.0
                    }, completion: { (complete) -> Void in
                        self.profilePhoto.image = nil
                        self.profilePhoto.transform = CGAffineTransformIdentity
                        self.profilePhoto.hidden = true
                })
            }
        }
        else {
            if let somePhoto = photo {
                profilePhoto.hidden = false
                profilePhoto.image = somePhoto
                addPhotoLabel.hidden = true
            }
            else {
                profilePhoto.hidden = true
                profilePhoto.image = nil
                addPhotoLabel.hidden = false
            }
        }
    }
    
    private func imagePickerControllerSourceTypeActionHandlerForSourceType(sourceType: UIImagePickerControllerSourceType) -> (action: UIAlertAction) -> Void {
        return { (action) in
            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.sourceType = sourceType
            imagePickerController.mediaTypes = [kUTTypeImage as String] // Import the MobileCoreServices framework to use kUTTypeImage (see top of file)
            imagePickerController.allowsEditing = true
            
            self.presentViewController(imagePickerController, animated: true, completion: nil)
        }
    }

    
    // MARK: Properties (Private)
    private var model: String?
    private var type: String?
    private var year: String?
    private var wheel: String?
    private var shifter: String?
    private var gears: String?
    private var photo: UIImage?
    
    private enum BikeRow {
        case Model
        case Type
        case Year
        case Wheel
        case Shifter
        case Gears
        case Photo
        
        static let Items: Array<BikeRow> = [.Model, .Type, .Year, .Wheel, .Shifter, .Gears, .Photo]
    }
}
