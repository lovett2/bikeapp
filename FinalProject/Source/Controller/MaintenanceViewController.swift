//
//  MaintenanceViewController.swift
//  FinalProject
//
//  Created by Kathryn Lovett on 8/1/16.
//
//

import UIKit

class MaintenanceViewController: UIViewController, BikeTouchViewDelegate {
    var tappedPart: Int?
    func bikeTouchViewWasTapped() {
        if let tappedBikePart = bikeTouchView.touchIndexPath {
            tappedPart = tappedBikePart
            performSegueWithIdentifier("InstructionSegue", sender: nil)
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        
        
//        guard let pointDataPath = NSBundle.mainBundle().pathForResource("paths", ofType: "plist"), let allPointValues = NSArray(contentsOfFile: pointDataPath) as! Array<Array<Dictionary<String, CGFloat>>> else {
//            fatalError("Could not load data")
//        }
        let pointDataPath = NSBundle.mainBundle().pathForResource("paths", ofType: "plist")!
        let allPointValues = NSArray(contentsOfFile: pointDataPath) as! Array<Array<Dictionary<String, CGFloat>>>
        
        var allPathValues: Array<Array<CGPoint>> = []
        for pointValues in allPointValues {
            var pathValues: Array<CGPoint> = []
            for singlePointValues in pointValues {
                let point = CGPoint(x: singlePointValues["x"]!, y: singlePointValues["y"]!)
                pathValues.append(point)
            }
            
            allPathValues.append(pathValues)
        }
        
        bikeTouchView.allPathValues = allPathValues
        bikeTouchView.delegate = self
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "InstructionSegue" {
            let instructionListViewController = segue.destinationViewController as! InstructionListViewController
            if let tapped = tappedPart {
                instructionListViewController.part = tapped
            }
        }
    }
    
    @IBOutlet private weak var bikeTouchView: BikeTouchView!
}
