//
//  BikeTouchView.swift
//  FinalProject
//
//  Created by Kathryn Lovett on 8/17/16.
//  Copyright © 2016 Kathryn Lovett. All rights reserved.
//


import UIKit


class BikeTouchView: UIView {
    weak var delegate: BikeTouchViewDelegate!
    
    var allPathValues: Array<Array<CGPoint>> = [] {
        didSet {
            allPaths.removeAll()
            
            for pathValues in allPathValues {
                let path = UIBezierPath()
                
                path.moveToPoint(pathValues[0])
                
                for point in pathValues[1..<pathValues.count] {
                    path.addLineToPoint(point)
                }
                
                path.closePath()
                
                allPaths.append(path)
            }
        }
    }
    private var allPaths: Array<UIBezierPath> = []
    var touchIndexPath: Int?
    
    @IBAction private func tap(sender: UITapGestureRecognizer) {
        let point = sender.locationInView(self)
        
        var pathIndex: Int? = nil
        for path in allPaths.enumerate() {
            if path.element.containsPoint(point) {
                pathIndex = path.index
                break
            }
        }
        
        if let somePathIndex = pathIndex {
            print("Touched path \(somePathIndex)")
            touchIndexPath = somePathIndex
            delegate.bikeTouchViewWasTapped()
        }
        
        
    }
}