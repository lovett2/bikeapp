//
//  BikeData.swift
//  FinalProject
//
//  Created by Kathryn Lovett on 8/2/16.
//
//

import CoreData
import CoreDataService
import Foundation

class BikeService {
    private init() {
        // Initialize core data
        guard let instructionDataPath = NSBundle.mainBundle().pathForResource("instructions", ofType: "plist"), let instructionData = NSArray(contentsOfFile: instructionDataPath) as? Array<Dictionary<String, AnyObject>> else {
            fatalError("Could not load data")
        }
        
        let fetchRequest = NSFetchRequest(entityName: Repair.entityName)
        let count = coreDataService.mainQueueContext.countForFetchRequest(fetchRequest, error: nil)
        guard count != NSNotFound else {
            fatalError("Could not count items in database")
        }
        
        if count == 0 {
            for item in instructionData {
                let repairEntity = NSEntityDescription.insertNewObjectForNamedEntity(Repair.self, inManagedObjectContext: coreDataService.mainQueueContext)
                
                repairEntity.name = item["name"] as? String
                repairEntity.instructions = item["instructions"] as? String
                repairEntity.type = item["type"] as? String
            }
            
            try! coreDataService.mainQueueContext.save()
            coreDataService.saveRootContext({
                // Intentionally left blank
            })
        }

    }
    
    func fetchedResultsControllerForRepairType(type: String) throws -> NSFetchedResultsController {
        let fetchRequest = NSFetchRequest(namedEntity: Repair.self)
        fetchRequest.predicate = NSPredicate(format: "type == %@", type)
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
        
        let context = coreDataService.mainQueueContext
        let resultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        try resultsController.performFetch()
        
        return resultsController
    }
    
    func fetchedResultsControllerForBikeProfileList() throws -> NSFetchedResultsController {
        let fetchRequest = NSFetchRequest(namedEntity: Bike.self)
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "model", ascending: true)]
        
        let context = coreDataService.mainQueueContext
        let resultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        try resultsController.performFetch()
        
        return resultsController
    }
    
    func updateBikeProfile(bike: Bike, model: String?, type: String?, year: String?, wheel: String?, shifter: String?, gears: String?, andPhoto photo: UIImage?) throws {
        let context = coreDataService.mainQueueContext
        if let someModel = model {
            bike.model = someModel
        }
        
        if let someType = type {
           bike.type = someType
        }
        
        if let someYear = year {
            if let intYear = Int(someYear) {
                bike.year = intYear
            }
        }
        
        if let someWheel = wheel {
            bike.wheel = someWheel
        }
        
        if let someShifter = shifter {
            bike.shifter = someShifter
        }
        
        if let someGears = gears {
            bike.gears = someGears
        }
        
        if let somePhoto = photo, somePhotoData = UIImageJPEGRepresentation(somePhoto, 1.0) {
            if let somePhotoEntity = bike.photo {
                somePhotoEntity.data = somePhotoData
            }
            else {
                let photoEntity = NSEntityDescription.insertNewObjectForNamedEntity(Photo.self, inManagedObjectContext: context)
                photoEntity.data = somePhotoData
                photoEntity.bike = bike
            }
        }
        else {
            if let somePhotoEntity = bike.photo {
                context.deleteObject(somePhotoEntity)
            }
        }
        
        try context.save()
        
        coreDataService.saveRootContext {
            print("Update bike save finished")
        }
    }
    
    func addBikeProfile(model: String, type: String, year: String?, andPhoto photo: UIImage?) throws {
        let context = coreDataService.mainQueueContext
        let bikeProfile = NSEntityDescription.insertNewObjectForNamedEntity(Bike.self, inManagedObjectContext: context)
        bikeProfile.model = model
        bikeProfile.type = type
        
        if let someYear = year {
            if let intYear = Int(someYear) {
                bikeProfile.year = intYear
            }
        }
        
        if let somePhoto = photo {
            if let somePhotoData = UIImageJPEGRepresentation(somePhoto, 1.0) {
                let photoEntity = NSEntityDescription.insertNewObjectForNamedEntity(Photo.self, inManagedObjectContext: context)
                photoEntity.data = somePhotoData
                photoEntity.bike = bikeProfile
            }
        }
        
        try context.save()
        
        coreDataService.saveRootContext {
 			print("Add bike profile save finished")
        }
    }
    
    static var sharedBikeService = BikeService()
    private let coreDataService = CoreDataService.sharedCoreDataService
}
