//
//  Repair.swift
//  FinalProject
//
//  Created by Kathryn Lovett on 8/21/16.
//
//

import Foundation
import CoreData
import CoreDataService


class Repair: NSManagedObject, NamedEntity {

    static var entityName: String {
        return "Repair"
    }

}
