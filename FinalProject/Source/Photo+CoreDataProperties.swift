//
//  Photo+CoreDataProperties.swift
//  FinalProject
//
//  Created by Kathryn Lovett on 8/20/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Photo {

    @NSManaged var data: NSData?
    @NSManaged var bike: Bike?
    @NSManaged var repair: Repair?

}
