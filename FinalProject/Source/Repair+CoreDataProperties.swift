//
//  Repair+CoreDataProperties.swift
//  FinalProject
//
//  Created by Kathryn Lovett on 8/21/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Repair {

    @NSManaged var instructions: String?
    @NSManaged var name: String?
    @NSManaged var type: String?
    @NSManaged var bike: Bike?
    @NSManaged var photo: NSSet?

}
