//
//  BikeTouchViewDelegate.swift
//  FinalProject
//
//  Created by Kathryn Lovett on 8/18/16.
//
//

import UIKit

protocol BikeTouchViewDelegate: class {
    func bikeTouchViewWasTapped()
}
