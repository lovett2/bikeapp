//
//  AddBikeProfileViewControllerDelegate.swift
//  FinalProject
//
//  Created by Kathryn Lovett on 8/12/16.
//
//

protocol AddBikeProfileViewControllerDelegate: class {
    func addBikeProfileViewControllerDidFinish()
}
