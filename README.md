# README #

### Repository for Bicycle Repair iOS Application ###

This repo contains the basis for a bike repair app for iOS. It provides a way to save bike information and look up simple repairs.

### Steps to Get Set Up ###

* Requirements
   * Xcode
   * Mac to run Xcode on
* Configuration
   * Clone repo onto your computer
   * `git clone https://lovett2@bitbucket.org/lovett2/bikeapp.git`
* How to run tests
   * There currently are not tests - these need to be created
* Deployment instructions
   * Once you have the repo locally, open up the project in Xcode
   * You can run the project on the simulator by pressing the play button
   * To export the app, first archive it, then open the archives organizer and press the export button

### Who do I talk to? ###

* Repo admin: Kathryn Lovett